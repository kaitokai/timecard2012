﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimeCard2012 {
    class InformationStrings {
        public const string notOnRecord = "The ID isn't on record, please contact the system administrator for help.";
        public const string breakIssues = "You currently cannot take a break.";
    }
}
