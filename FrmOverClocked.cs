﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TimeCard2012 {
    public partial class FrmOverClocked : Form {
        private const int HH = 0;
        private const int mm = 1;

        public FrmOverClocked() {
            InitializeComponent();
        }// end FrmOverClocked

        private void btnCancel_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
        }// end btnCancel_Click

        private void btnStaff_Click(object sender, EventArgs e) {
            if (txtbTime.Text != "") {
                int[] _time = Hours.custConvert(txtbTime.Text);
                if (rbtnPM.Checked) {
                    if (!(12 < _time[HH])) {
                        _time[HH] += 12;
                    }
                }
                
                Tag = _time[HH] + ":" + _time[mm];
                this.DialogResult = DialogResult.OK;
            }
            else {
                btnCancel_Click(sender, e);
            }
        }// end btnStaff_Click routine

    }// end form declaration
}// end namespace
