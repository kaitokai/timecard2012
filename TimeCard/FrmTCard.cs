﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using TimeCard2012;

namespace TimeCard2012.TimeCard {
    public partial class FrmTCard : Form {
        // FIELDS:
        private string employeeName = null;
        private string idNumber = ""; // ADDED: 12/30/2011
        private string cssName = "temp.css"; // ADDED: 4/3/2012
        private int startMonth, endMonth, startYear, endYear;
        private bool displayAll = false;

        public FrmTCard(string employeeName, string idNumber, int month, int year, bool displayAll) {
            this.employeeName = employeeName;
            this.idNumber = idNumber; // ADDED: 12/30/2011
            this.startMonth = month;
            this.endMonth = month;
            this.startYear = year;
            this.endYear = year;
            this.displayAll = displayAll;

//            this.path += id + "hours.xml";
            InitializeComponent();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e) {
            Graphics g = e.Graphics;
            SolidBrush Brush = new SolidBrush(Color.Black);

            string printText = employeeName + rtbTime.Text;

            g.DrawString(printText, new Font("arial", 12), Brush, 10, 10);
        }

        private void FrmTCard_Load(object sender, EventArgs e) {
            lblEmployeeName.Text = employeeName;
            if (displayAll) {
                lblDates.Text = "All time.";
            }
            else {
                lblDates.Text = startMonth + "/" + startYear;
            }
            
            rtbTime.AppendText("Date\t\tStart Time\tEnd Time\tTotal Time(Hours)\n");
            // ADDED: 12/30/2011
            List<Hours> times = Employee.GetHours(idNumber);
            List<Time> listTime = new List<Time>();
            double cummulativeTime = 0;
            int days = 0;
            int year, month, day;
            string clockIn, clockOut;

            foreach (Hours time in times) {
                year = time.TimeIn.Year;
                month = time.TimeIn.Month;
                day = time.TimeIn.Day;
                listTime.Add(new Time(year, month, day));

                clockIn = time.TimeIn.ToString("HH:mm");
                clockOut = time.TimeOut.ToString("HH:mm");

                if (displayAll) {
                    rtbTime.AppendText(month + "/" + day + "/" + year + "\t" + clockIn + "\t\t" +
                        clockOut + "\t\t" + time.TotalTime.ToString("F2") + "\n");
                    cummulativeTime += time.TotalTime;
                }
                else if (startYear == year && startMonth == month) {
                    rtbTime.AppendText(month + "/" + day + "/" + year + "\t" + clockIn + "\t\t" + 
                        clockOut + "\t\t" + time.TotalTime.ToString("F2") + "\n");
                    cummulativeTime += time.TotalTime;
                }
                days = Time.UniqueDays(listTime);
            }
            lblHours.Text = cummulativeTime.ToString("F2");
            lblDaysWorked.Text = days.ToString();
            // END ADDED
        }//

        class Time {
            // FIELDS:
            private int year, month, day;

            // CONSTRUCTORS:
            public Time(int year, int month, int day) {
                this.year = year;
                this.month = month;
                this.day = day;
            }

            // PROPERTIES:
            public int Day {
                get {
                    return day;
                }
            }
            // ROUTINES: 
            public bool Equals(Time time) {
                return (this.year == time.year)
                    && (this.month == time.month)
                    && (this.day == time.day);
            }// end routine Equals
            public static int UniqueDays(List<Time> listTime) {
                // to get each unique number
                List<Time> _listTime = listTime;
                List<int> te = new List<int>();
                for (int index = 0; index < listTime.Count; index++) {
                    if (!te.Contains(_listTime[index].Day)) {
                        te.Add(_listTime[index].Day);
                    }
                }
                return te.Count;
            }// end routine UniqueDays
        }
    }
}
