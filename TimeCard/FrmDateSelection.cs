﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TimeCard2012.TimeCard {
    public partial class FrmDateSelection : Form {
        public FrmDateSelection() {
            InitializeComponent();
        }

        private void FrmDateSelection_Load(object sender, EventArgs e) {

        }
        private bool displayAll = false;
        private void btnOK_Click(object sender, EventArgs e) {
            if (txtbYear.Text != "" && txtbMonth.Text != "" && txtbIDNumber.Text != null) {
                Employee employee = null;
                try{
                    int month = Convert.ToInt32(txtbMonth.Text);
                    int year = Convert.ToInt32(txtbYear.Text);
                    foreach (Employee _employee in Employee.EmployeeList) {
                        if (txtbIDNumber.Text.Equals(_employee.IDNumber)) {
                            employee = _employee;
                            break;
                        }
                    }
                    if (employee != null) {
                        FrmTCard tCard = new FrmTCard(employee.Name, employee.IDNumber, month, year, displayAll);
                        this.DialogResult = DialogResult.OK;
                        tCard.ShowDialog();
                    }
                    else {
                        MessageBox.Show(InformationStrings.notOnRecord);
                        clearFields();
                    }
                }catch{
                    MessageBox.Show("Please use proper formatting.");
                }
            }
            else {
                MessageBox.Show("Please fill each of the text fields.", "Error");
            }
        }

        private void clearFields() {
            txtbIDNumber.Text = "";
            txtbMonth.Text = "";
            txtbYear.Text = "";
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnThisMonth_Click(object sender, EventArgs e) {
            txtbMonth.Text = Convert.ToString(DateTime.Now.Month);
            txtbYear.Text = Convert.ToString(DateTime.Now.Year);
            btnOK_Click(this, e);
        }

        private void btnAll_Click(object sender, EventArgs e) {
            displayAll = true;
            btnThisMonth_Click(this, e);
        }

    }
}
