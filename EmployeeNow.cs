﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Windows.Forms;

namespace TimeCard2012 {
    class EmployeeNow : INotifyPropertyChanged{
        public event PropertyChangedEventHandler PropertyChanged;

        // FIELDS (MEMBER VARIABLES):
        private string idNumber; // ID number is the last four digits of your SSN.
        private string name;

        private DateTime now;
        private DateTime later;
        private TimeSpan breakTime;

        private double totalTime = 0.00;
        private bool doneWithCycle = false;
        private bool isOnBreak = false;

        // CONSTRUCTORS:
        private EmployeeNow(string id, string name, DateTime now) {
            this.idNumber = id;
            this.name = name;
            this.now = now;
            later = new DateTime();
            breakTime = new TimeSpan();
        }// end cons EmployeeNow

        // PROPERTIES:
        public string Name {
            get {
                return name;
            }
        }// end Property Name
        public string ClockIn {
            get {
                return now.ToShortTimeString();
                
            }
        }// end Property ClockIn
        public string ClockOut {
            get {
                // check to see if the date is 12:00 AM 0000, if it is return a blank string
                if (later == new DateTime()) {
                    return "";
                }
                return later.ToShortTimeString();
            }
        }// end Property ClockOut
        public TimeSpan BreakTime {
            get {
                return breakTime;
            }
            set {
                breakTime = value;
                NotifyPropertyChanged("BreakTime");
            }
        }// end Property BreakTime
        
        public string TotalTime {
            get {
                return totalTime.ToString("F2");
            }
            set {
                totalTime = Convert.ToDouble(value);
                NotifyPropertyChanged("TotalTime");
            }
        }// end Property TotalTime

        // ROUTINES:
        public string getId() {
            return idNumber;
        }// end routine getID

        public DateTime GetDTClockOut() {
            return later;
        }// end routine GetDTClockOut
        public void SetClockOut(DateTime later) {
            this.later = later;
            NotifyPropertyChanged("ClockOut");
        }// end routine SetClockOut

        public DateTime GetDTClockIn() {
            return now;
        }// end routine GetDTClockIn

        public bool IsOnBreak() {
            return isOnBreak;
        }// end routine IsOnBreak
        public void SetOnBreak(bool isOnBreak) {
            this.isOnBreak = isOnBreak;
        }// end routine SetOnBreak

        public bool IsDone() {
            return doneWithCycle;
        }// end routine GetDone
        public void SetDone(bool done) {
            this.doneWithCycle = done;
        }// end routine SetDone

        private void NotifyPropertyChanged(string name) {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }// end NotifyPropertyChanged
    
        // STATIC FIELDS:
        private static BindingList<EmployeeNow> onBoardList = new BindingList<EmployeeNow>();
        public static BindingList<EmployeeNow> EmployeeRoster {
            get {
                GatherTempHours();
                return onBoardList;
            }
        }

        // STATIC ROUTINES:
        public static Status ClockInOutEmployee(string idNumber) {
//            int TIME_OVER_CLOCKED = 13;
//            string _IDNumber = idNumber;
            // check to see if the employee exists at all
//            Employee _employee = Employee.GetEmployee(_IDNumber);
            Employee _employee = Employee.GetEmployee(idNumber);
            if (_employee != null) {
                // if the employee exists, create a temporary EmployeeNow object
//                EmployeeNow _employeeNow = GetEmployeeOnClock(_IDNumber);
                EmployeeNow _employeeNow = GetEmployeeOnClock(idNumber);

                // if the employee is logged in
                if (_employeeNow != null) {
                    // we are also checking for redundancies, 
                    // in case we want to log in multiple times during the work day
                    if (_employeeNow.IsOnBreak()) {
                        MessageBox.Show("Please return from break", "You're still on break!s");
                        return Status.BREAK; // change to RETURN STATUS.BREAK
                    }
                    DateTime _hours = new DateTime();
//                    int timeDifference = (DateTime.Now.Hour - _employeeNow.GetDTClockIn().Hour);
                    int timeDifference = DateTime.Now.Subtract(_employeeNow.GetDTClockIn()).Days;
//                    if (timeDifference < TIME_OVER_CLOCKED) {
                    if(timeDifference == 0){
                        _hours = DateTime.Now;
                    }
                    else { // if the employee is clocked in for over the time defined in TIME_OVER_CLOCKED
                        // then prompt the employee that they may have stayed clocked in unintentionally
                        FrmOverClocked overClocked = new FrmOverClocked();
                        DialogResult _tempResult = overClocked.ShowDialog();

                        if (_tempResult == DialogResult.OK) {
                            try {
                                int[] time = Hours.custConvert((string)overClocked.Tag);
                                _hours = _employeeNow.GetDTClockIn();
                                _hours = _hours.AddHours(-(_hours.Hour) + time[0]);
                                _hours = _hours.AddMinutes(-(_hours.Minute) + time[1]);
                            } catch {
                                MessageBox.Show("Please enter a valid format as described in the dialog above.");
                                return Status.UNRESOLVED;
                            }
                        }
                        else {// this issue was not resolved, so lets just abandon the rest of this
                            return Status.UNRESOLVED;// RETURN STATUS.UNRESOLVED;
                        }
                    }
                    _employeeNow.SetClockOut(_hours);
                    
                    // this portion of code deals with the completion of hours by the employee by adding their hours together
                    Hours hours = new Hours(_employeeNow);
                    _employee.AddHours(hours);
                    _employeeNow.TotalTime = Convert.ToString(hours.TotalTime);
                    _employeeNow.SetDone(true);
                    CreateTempClocks();
                    return Status.LOGGEDOUT;
                }
                // otherwise, let's create a new time slot for the employee
                else { 
                    EmployeeNow _tempNow = new EmployeeNow(_employee.IDNumber, _employee.Name, DateTime.Now);
//                    employeeLogIn.Add(_tempNow);
                    onBoardList.Add(_tempNow);
                    CreateTempClocks();
                    return Status.LOGGEDIN;
                }
            }// end
            return Status.DOES_NOT_EXIST;
        }// end routine login/out

        public static EmployeeNow GetEmployeeOnClock(string _IDNumber) {
            EmployeeNow _employeeNow = null;
            foreach (EmployeeNow employee in onBoardList) {
                if (employee.getId().Equals(_IDNumber) && !employee.IsDone()) {
                    _employeeNow = employee;
                    break;
                }
            }
            return _employeeNow;
        }// end routine GetEmployeeOnClock

        private static string TEMP_CLOCK = @"../../Temp/_clocks.temp";
        private static void CreateTempClocks(){
            string directory = @"../../Temp";
            if (!Directory.Exists(directory)) {
                Directory.CreateDirectory(directory);
            }
            File.Delete(TEMP_CLOCK);
            FileStream fileStream = new FileStream(TEMP_CLOCK, FileMode.OpenOrCreate);
            StreamWriter writer = new StreamWriter(fileStream);

            foreach (EmployeeNow employee in onBoardList) {
                if (!employee.IsDone()) {
                    writer.WriteLine(employee.getId() + "|" + employee.Name + "|" + employee.GetDTClockIn());
                }
            }
            writer.Flush();
            writer.Close();
            fileStream.Close();
        }// end routine CreateTempClocks
        private static void GatherTempHours() {
            // Duplicated code, must change
            string directory = @"../../Temp";
            if (!Directory.Exists(directory)) {
                Directory.CreateDirectory(directory);
            }
            FileStream fileStream = new FileStream(TEMP_CLOCK, FileMode.Open);
            using (StreamReader reader = new StreamReader(fileStream)) {
                string line;
                string[] test;
                while ((line = reader.ReadLine()) != null) {
                    test = line.Split('|');// delimiter
                    DateTime dt = DateTime.Parse(test[2]);
                    onBoardList.Add(new EmployeeNow(test[0], test[1], dt));
                }
            }
            fileStream.Close();
        }
    }// end class EmployeeNow
    public enum Status {
        BREAK,
        UNRESOLVED,
        LOGGEDIN,
        LOGGEDOUT,
        DOES_NOT_EXIST,
        OVER_CLOCKED
    }// end enum Status

}// end namespace location
