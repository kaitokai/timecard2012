﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using TimeCard2012.TimeCard;

namespace TimeCard2012 {
    public partial class FrmMain : Form {
        // FIELDS:
        
        // CONSTRUCTORS:
        public FrmMain() {
            InitializeComponent();
        }// end constructor FrmMain

        // ROUTINES:
        private void Form1_Load(object sender, EventArgs e) {
            Employee.InitializeEmployeeList();
            dgvEmployees.DataSource = EmployeeNow.EmployeeRoster;
            FileStream optionsFile = new FileStream(@"..\..\settings.txt", FileMode.OpenOrCreate);
            
        }// end routine Load

        //********************************************************************
        private void btnCreate_Click(object sender, EventArgs e) {
            FrmSetup setup = new FrmSetup();
            if (setup.ShowDialog() == DialogResult.OK) {
            }
        }// end routine btnCreate_Click

        //********************************************************************
        private void btnClockOption_Click(object sender, EventArgs e) {
            string[] clockinTitles = {"Clocking hell...", "Click' Clock, You're on the Clock!",
                                         "C-c-c-clockin' in!!", "Cluckin'?", "Clockinspheil"};

            DateTime initalTime = new DateTime();
            if (initalTime == new DateTime()) {
                initalTime = DateTime.Now;
            }
            else {
                int timeDifference = (initalTime.Day - DateTime.Now.Day);
                if (timeDifference <= 0) {  // if the number is negative they are overclocked
                    foreach (EmployeeNow _employeeNow in EmployeeNow.EmployeeRoster) {
                        if (_employeeNow.IsDone()) {
                            EmployeeNow.EmployeeRoster.Remove(_employeeNow);
                        }
                    }
                }
            }// this portion of code determines if the person logged out or not and clears the employee out
            // after 24 hours.

            FrmLogin login = new FrmLogin(
                clockinTitles[new Random().Next() % clockinTitles.Length]);
            if (login.ShowDialog() == DialogResult.OK) {    // Check if the user clicked OK
                string _idNumber = (string)login.Tag;
                Status status = EmployeeNow.ClockInOutEmployee(_idNumber);
                switch (status) {
                    case Status.BREAK:
                        break;
                    case Status.DOES_NOT_EXIST:
                        MessageBox.Show(InformationStrings.notOnRecord, "Account error...");
                        break;
                    case Status.LOGGEDIN:
                        break;
                    case Status.LOGGEDOUT:
                        MessageBox.Show("Have a nice day " + Employee.GetEmployee(_idNumber).Name + " :)", "Have a nice day!");
                        break;
                    case Status.UNRESOLVED:
                        break;
                    default:
                        break;
                }
            }// end DialogResult.OK IF
        }// end routine CLOCK

        //*******************BREAK BUTTON**************************
        private void btnBreak_Click(object sender, EventArgs e) {
            string[] breakTitles = {"Taking a Break...", "Breaking Legs...", "Breaking Bad...",
                                       "Kit-Kat'in", "Breaking stuff..."};

            FrmLogin breakLogin = new FrmLogin(breakTitles[(new Random().Next() % breakTitles.Length)]);
            if (breakLogin.ShowDialog() == DialogResult.OK) {
                string _IDNumber = (string)breakLogin.Tag;
                Employee _employee = Employee.GetEmployee(_IDNumber); //GetExistingEmployee(_IDNumber);
                if (_employee != null) {
                    EmployeeNow _employeeNow = EmployeeNow.GetEmployeeOnClock(_IDNumber); //GetEmployeeOnClock(_IDNumber);
                    // Making sure that the employee exists and isn't logged out.
                    if (_employeeNow != null && _employeeNow.ClockOut == "") {
                        if (_employeeNow.IsOnBreak()) {
                            TimeSpan _timeSpan = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                            _employeeNow.BreakTime = _timeSpan.Subtract(_employeeNow.BreakTime);
                            _employeeNow.SetOnBreak(false);
                        }
                        else { // if the employee is not on a break, let's put them on a break
                            TimeSpan _timeSpan = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                            _employeeNow.BreakTime = _timeSpan;
                            _employeeNow.SetOnBreak(true);
                        }
                    }
                    else {
                        MessageBox.Show(InformationStrings.breakIssues);
                    }
                }
                else {
                    MessageBox.Show(InformationStrings.notOnRecord);
                }
            }
        }// end routine btnBreak_Click
        
        //*******************HOURS BUTTON**************************
        private void btnTimeCard_Click(object sender, EventArgs e) {
            FrmDateSelection dateSelection = new FrmDateSelection();
            dateSelection.ShowDialog();
        }

        private void btnStaff_Click(object sender, EventArgs e) {
            FrmLogin staffLogin = new FrmLogin("Staff Login");
            DialogResult _tempResult = staffLogin.ShowDialog();

            if (_tempResult == DialogResult.OK) {
                string _idNumber = Convert.ToString(staffLogin.Tag);
                Employee _employeeCheck = Employee.GetEmployee(_idNumber);
                if (_employeeCheck.EmployeeType == Classification.STAFF) {
                    // STAFF ideas... the id number is the password for the staff accounts
                    // as opposed to the employee id number for loggin in. the system
                    // will be the same in principal only done with an RSA encoding system
                    // in order to keep the staff id numbers unknown. eventually we would like
                    // to mask all employee information as well. perhaps even change to SQL
                    // database as opposed to an open xml system.
                }
                else {

                }
            }
        }// end routine btnTimeCard_Click

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e) {
            AboutBox1 aBox = new AboutBox1();
            aBox.ShowDialog();
        }

        // PROPERTIES:
    }// end class FrmMain
}// end namespace
