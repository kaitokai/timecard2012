﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TimeCard2012 {
    public partial class FrmSetup : Form {
        // FIELDS:

        // CONSTRUCTORS:
        public FrmSetup() {
            InitializeComponent();
        }

        // ROUTINES:
        private void btnClear_Click(object sender, EventArgs e) {
            txtbFirstName.Text = "";
            txtbMiddle.Text = "";
            txtbLastName.Text = "";
            txtbIdNumber.Text = "";
            txtbPhoneNo.Text = "";
            txtbEmail.Text = "";
            txtbDescription.Text = "";
        }

        private bool QualifyingFields() {
            int idDigits = 4;
            return (txtbDescription.Text != ""
                || txtbFirstName.Text != ""
                || txtbIdNumber.Text != ""
                || txtbLastName.Text != "" 
                || txtbPhoneNo.Text != ""
                && txtbIdNumber.Text.Length == idDigits);
        }
        private void btnCreate_Click(object sender, EventArgs e) {
            if (QualifyingFields()) {
                string name = txtbFirstName.Text;
                if (txtbMiddle.Text != "") {
                    name += " " + txtbMiddle.Text;
                }
                name += " " + txtbLastName.Text;
                string idNumber = txtbIdNumber.Text;
                string phoneNumber = txtbPhoneNo.Text;
                string email = txtbEmail.Text;
                string description = txtbDescription.Text;
                Classification classification = Classification.DEFAULT;
                if (rbHourly.Checked) {
                    classification = Classification.HOURLY;
                }
                else if (rbWorkStudy.Checked) {
                    classification = Classification.WORK_STUDY;
                }
                else if (rbContract.Checked) {
                    classification = Classification.CONTRACTOR;
                }

                Employee.Create(name, idNumber, phoneNumber, email, classification, description);
                this.DialogResult = DialogResult.OK;
            }
            else {
                MessageBox.Show("All fields must be filled, with the exception of the Middle Initial.");
            }
        }
        
        private void btnCancel_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
        }

        private void FrmSetup_Load(object sender, EventArgs e) {

        }
    }
}
