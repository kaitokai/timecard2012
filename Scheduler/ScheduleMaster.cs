﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Xml;

namespace TimeCard2012.Scheduler {
    class ScheduleMaster : BindingList<ScheduleElement>{
        private const string path = @"../../schedule.xml";
        public ScheduleMaster() : this(path){}
        public ScheduleMaster(string path) {
            FileStream fileStream = new FileStream(path, FileMode.OpenOrCreate);
            XmlReaderSettings settings = new XmlReaderSettings() {
                IgnoreComments = true,
                IgnoreWhitespace = true
            };
            XmlReader xmlin = XmlReader.Create(fileStream, settings);
            if(xmlin.ReadToDescendant("Schedule")){
                do {
                    
                } while (xmlin.ReadToNextSibling("Schedule"));
            }
        }// end cons ScheduleMaster
        private class ScheduleElement {
            private string mondayHours;
            private string tuesdayHours;
            private string wednesdayHours;
            private string thursdayHours;
            private string fridayHours;
            private string saturdayHours;

            public ScheduleElement(string mondayHours, string tuesdayHours, string wednesdayHours, string thursdayHours, string fridayHours) {
                this.mondayHours = mondayHours;
                this.tuesdayHours = tuesdayHours;
                this.wednesdayHours = wednesdayHours;
                this.thursdayHours = thursdayHours;
                this.fridayHours = fridayHours;
            }

            public string MondayHours {
                get {
                    return mondayHours;
                }
            }
            public string TuesdayHours {
                get {
                    return tuesdayHours;
                }
            }
            public string WednesdayHours {
                get {
                    return wednesdayHours;
                }
            }
            public string ThursdayHours {
                get {
                    return thursdayHours;
                }
            }
            public string FridayHours {
                get {
                    return fridayHours;
                }
            }
        }// end class schedule element

    }// end class ScheduleMaster
    class ScheduleElement {
        private string mondayHours;
        private string tuesdayHours;
        private string wednesdayHours;
        private string thursdayHours;
        private string fridayHours;
        private string saturdayHours;

        public ScheduleElement(string mondayHours, string tuesdayHours, string wednesdayHours, string thursdayHours, string fridayHours) {
            this.mondayHours = mondayHours;
            this.tuesdayHours = tuesdayHours;
            this.wednesdayHours = wednesdayHours;
            this.thursdayHours = thursdayHours;
            this.fridayHours = fridayHours;
        }

        public string MondayHours {
            get {
                return mondayHours;
            }
        }
        public string TuesdayHours {
            get {
                return tuesdayHours;
            }
        }
        public string WednesdayHours {
            get {
                return wednesdayHours;
            }
        }
        public string ThursdayHours {
            get {
                return thursdayHours;
            }
        }
        public string FridayHours {
            get {
                return fridayHours;
            }
        }
    }// end class schedule element
}// end namespace
