﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.IO;

namespace TimeCard2012 {
    class Employee {
        // CONSTRUCTORS:
        private Employee(string name, string idNumber, string phoneNumber, string emailAddress, Classification classification, string description) {
            this.name = name;
            this.idNumber = idNumber;
            this.phoneNumber = phoneNumber;
            this.emailAddress = emailAddress;
            this.classification = classification;
            this.description = description;

            hours = GetHours(this);
        }// end cons Employee
        
        // COVERING FIELDS:
        private string name;
        private string idNumber;
        private string phoneNumber;
        private string emailAddress;
        private Classification classification;
        private string description;
        private List<Hours> hours = null;

        // PROPERTIES:
        public string Name {
            get {
                return name;
            }
            set {
                name = value;
            }
        }
        public string IDNumber {
            get {
                return idNumber;
            }
            set {
                idNumber = value;
            }
        }
        public Classification EmployeeType {
            get {
                return classification;
            }
            set {
                classification = value;
            }
        }
        public string PhoneNumber {
            get {
                return phoneNumber;
            }
            set {
                phoneNumber = value;
            }
        }// end 
        public string EmailAddress {
            get {
                return emailAddress;
            }
            set {
                emailAddress = value;
            }
        }
        public string Description {
            get {
                return description;
            }
            set {
                description = value;
            }
        }
        public List<Hours> Times {
            get {
                return hours;
            }
            set {
                hours = value;
            }
        }

        // ROUTINES:

        // STATIC ROUTINES:
        private const string employeePath = @"../../Employee.xml";
        private const string hoursPath = @"../../Hours/";
        private const string hoursExtension = "hours.xml";
        public void AddHours(Hours time) {
            Times.Add(time);
            XmlWriterSettings settings = new XmlWriterSettings() {
                Indent = true,
                IndentChars = "\t"
            };
            XmlWriter xmlOut = null;
            string hoursLocation = hoursPath + IDNumber + hoursExtension;
            Directory.CreateDirectory(hoursPath);
            FileStream fileStream = new FileStream(hoursLocation, FileMode.OpenOrCreate);
            try {
                xmlOut = XmlWriter.Create(fileStream, settings);
                xmlOut.WriteStartDocument();
                xmlOut.WriteStartElement("Hours");
                string month, day, year;
                foreach (Hours times in Times) {
                    month = times.TimeIn.Month.ToString();
                    day = times.TimeIn.Day.ToString();
                    year = times.TimeIn.Year.ToString();

                    xmlOut.WriteStartElement("Hour");
                    xmlOut.WriteAttributeString("month", month);
                    xmlOut.WriteAttributeString("day", day);
                    xmlOut.WriteAttributeString("year", year);

                    xmlOut.WriteElementString("Clocked_In", times.TimeIn.ToString("HH:mm"));
                    xmlOut.WriteElementString("Clocked_Out", times.TimeOut.ToString("HH:mm"));
                    xmlOut.WriteElementString("Break_Time", times.BreakTime.ToString("c"));
                    xmlOut.WriteElementString("Total_Time", times.TotalTime.ToString());
                    xmlOut.WriteEndElement();
                }
                xmlOut.WriteEndElement();
                xmlOut.WriteEndDocument();
            } catch (FormatException ee){
                MessageBox.Show(ee.Message);
            } finally {
                if (xmlOut != null) {
                    xmlOut.Close();
                }
                fileStream.Close();
            }
        }// end AddHours
        public void AddHours(List<Hours> time) {
            foreach (Hours times in time) {
                AddHours(times);
            }
        }

        private static List<Employee> employeeList = new List<Employee>();
        public static List<Employee> EmployeeList {
            get {
                return employeeList;
            }
        }

        public static void Create(string name, string idNumber, string phoneNumber, string emailAddress, Classification classification, string description){
            Employee _employee = new Employee(name, idNumber, phoneNumber, emailAddress, classification, description);
            Create(_employee);
        }
        public static void Create(Employee employee) {
            bool isUniqueAccount = true;
            foreach (Employee _employee in employeeList) {
                if (_employee.IDNumber.Equals(employee.IDNumber)) {
                    isUniqueAccount = false;
                }
            }
            if (isUniqueAccount) {
                MessageBox.Show("Welcome to the the ACC lab " + employee.Name + "!\n" +
                    "The last four digits of your SSN is your ID, which is used to clock in and clock out.", "Welcome to the ACC Lab");
                employeeList.Add(employee);
                WriteEmployeeToXml(employeeList);
            }
            else {
                MessageBox.Show("This account already exists within the database, \n" +
                    "contact the system administrator to resolve any issues.");
            }
        }

        public static Employee GetEmployee(string _IDNumber) {
            Employee _employee = null;
            foreach (Employee employee in employeeList) {
                if (employee.IDNumber.Equals(_IDNumber)) {
                    _employee = employee;
                }
            }
            return _employee;
        }// end properties GetExistingEmployee

        private static XmlReaderSettings settings = new XmlReaderSettings() {
            IgnoreComments = true,
            IgnoreWhitespace = true
        };
        public static List<Hours> GetHours(string idNumber) {
            // Gather hours unique to this current employee
            // The code below is VERY messy, some code re-work is highly recommended.
            string hoursLocation = hoursPath + idNumber + hoursExtension;
            if (File.Exists(hoursLocation)) {
                List<Hours> _tempHours = new List<Hours>();
                XmlReader xmlHours = XmlReader.Create(hoursLocation, settings);
                if (xmlHours.ReadToDescendant("Hour")) {
                    do {
                        int month = Convert.ToInt32(xmlHours["month"]);
                        int day = Convert.ToInt32(xmlHours["day"]);
                        int year = Convert.ToInt32(xmlHours["year"]);

                        xmlHours.ReadStartElement("Hour");
                        string clockedIn = xmlHours.ReadElementContentAsString();
                        string clockedOut = xmlHours.ReadElementContentAsString();
                        string breakTime = xmlHours.ReadElementContentAsString();
                        double totalTime = xmlHours.ReadElementContentAsDouble();

                        Hours _tempHour = new Hours(month, day, year, clockedIn, clockedOut, breakTime);
                        _tempHour.TotalTime = totalTime;

                        _tempHours.Add(_tempHour);
                    } while (xmlHours.ReadToNextSibling("Hour"));
                }
                xmlHours.Close();
//                _tempEmployee.AddHours(_tempHours);
                return _tempHours;
            }
            return new List<Hours>();
        }
        public static List<Hours> GetHours(Employee employee) {
            return GetHours(employee.IDNumber);
        }

        public static void InitializeEmployeeList(){
            List<Employee> _temp = new List<Employee>();
            if (!File.Exists(employeePath)) {
                // ADDED: 12/30/2011
                FileStream fileStream = new FileStream(employeePath, FileMode.OpenOrCreate);
                XmlWriterSettings wSettings = new XmlWriterSettings() {
                    Indent = true,
                    IndentChars = "\t"
                };

                XmlWriter xmlOut = XmlWriter.Create(fileStream, wSettings);
                xmlOut.WriteStartDocument();
                xmlOut.WriteStartElement("Employees");
                xmlOut.WriteEndElement();
                xmlOut.WriteEndDocument();
            }
            XmlReader xmlIn = null;
            try {
                xmlIn = XmlReader.Create(employeePath, settings); // MODIFIED: 12/30/2011
                if (xmlIn.ReadToDescendant("Employee")) {
                    do {
                        string idNumber = xmlIn["id"];
                        xmlIn.ReadStartElement("Employee");
                        string name = xmlIn.ReadElementContentAsString();
                        Classification type = Classification.DEFAULT;
                        switch (xmlIn.ReadElementContentAsString()) {
                            case "HOURLY":
                                type = Classification.HOURLY;
                                break;
                            case "WORK_STUDY":
                                type = Classification.WORK_STUDY;
                                break;
                            case "CONTRACTOR":
                                type = Classification.CONTRACTOR;
                                break;
                            case "STAFF":
                                type = Classification.STAFF;
                                break;
                            default:
                                break;
                        }
                        string phoneNumber = xmlIn.ReadElementContentAsString();
                        string emailAddress = xmlIn.ReadElementContentAsString();
                        string description = xmlIn.ReadElementContentAsString();

                        Employee _tempEmployee = new Employee(name, idNumber, phoneNumber, emailAddress, type, description);
                        List<Hours> _times = GetHours(_tempEmployee);
                        if (_times != null) {
                            _tempEmployee.Times = _times;
                        }
                        _temp.Add(_tempEmployee);
                        // Write in code for hours per employee
                    } while (xmlIn.ReadToNextSibling("Employee"));
                }
            } catch (Exception ex){
                MessageBox.Show(ex.ToString());
            } finally {
                if (xmlIn != null)
                    xmlIn.Close();
            }
            employeeList = _temp;
        }// end InitalizeEmployeeList

        public static void WriteEmployeeToXml(List<Employee> employees) {
            XmlWriterSettings settings = new XmlWriterSettings() {
                Indent = true,
                IndentChars = "\t"
            };

            XmlWriter xmlOut = XmlWriter.Create(employeePath, settings);
            xmlOut.WriteStartDocument();
            xmlOut.WriteStartElement("Employees");
            foreach (Employee employee in employees) {
                xmlOut.WriteStartElement("Employee");
                xmlOut.WriteAttributeString("id", employee.IDNumber);

                xmlOut.WriteElementString("Name", employee.Name);
                xmlOut.WriteElementString("Classification", employee.EmployeeType.ToString());
                xmlOut.WriteElementString("PhoneNumber", employee.PhoneNumber);
                xmlOut.WriteElementString("E-Mail", employee.EmailAddress);
                xmlOut.WriteElementString("JobDescription", employee.Description);
                xmlOut.WriteEndElement();
            }
            xmlOut.WriteEndElement();
            xmlOut.Close();
        }
    }// end class Employee

    class Hours {
        private DateTime timeIn;
        private DateTime timeOut;
        private TimeSpan breakTime;
        private double totalTime;

        public Hours(int month, int day, int year, string timein, string timeout, string breakTime) {
            int[] hoursIn = custConvert(timein);
            int[] hoursOut = custConvert(timeout);

            timeIn = new DateTime(year, month, day, hoursIn[0], hoursIn[1], 0);
            timeOut = new DateTime(year, month, day, hoursOut[0], hoursOut[1], 0);

            TimeSpan time = timeOut.Subtract(timeIn);
            totalTime = (double)time.Hours;
            totalTime += ((double)time.Minutes / 60);

            BreakTime = new TimeSpan(custConvert(breakTime)[0], custConvert(breakTime)[1], 0);
        }
        public Hours(EmployeeNow _employeeNow) : 
            this(_employeeNow.GetDTClockOut().Month, 
            _employeeNow.GetDTClockOut().Day, 
            _employeeNow.GetDTClockOut().Year, 
            _employeeNow.GetDTClockIn().ToString("HH:mm"), 
            _employeeNow.GetDTClockOut().ToString("HH:mm"), 
            _employeeNow.BreakTime.ToString("c"))
        {}

        public static int[] custConvert(string str) {
            string[] _temp = str.Split(':');
            return Array.ConvertAll<string, int>(_temp, new Converter<string, int>(StrToInt));
        }
        private static int StrToInt(string str) {
            return Convert.ToInt32(str);
        }

        public DateTime TimeIn {
            get {
                return timeIn;
            }
        }

        public DateTime TimeOut {
            get {
                return timeOut;
            }
        }

        public double TotalTime {
            get {
                return totalTime;
            }
            set {
                totalTime = value;
            }
        }

        public TimeSpan BreakTime {
            get {
                return breakTime;
            }
            set {
                breakTime = value;
                totalTime -= breakTime.Hours;
                totalTime -= (double)(breakTime.Minutes / 60);
            }
        }
    }// end class Hours
    enum Classification {
        DEFAULT,
        HOURLY,
        WORK_STUDY,
        CONTRACTOR,
        STAFF
    }
}
