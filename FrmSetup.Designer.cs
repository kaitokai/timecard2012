﻿namespace TimeCard2012 {
    partial class FrmSetup {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.txtbIdNumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbFirstName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtbMiddle = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtbLastName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbPhoneNo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtbDescription = new System.Windows.Forms.TextBox();
            this.rbWorkStudy = new System.Windows.Forms.RadioButton();
            this.rbHourly = new System.Windows.Forms.RadioButton();
            this.rbContract = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtbEmail = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Last four digits SSN:";
            // 
            // txtbIdNumber
            // 
            this.txtbIdNumber.Location = new System.Drawing.Point(120, 35);
            this.txtbIdNumber.Name = "txtbIdNumber";
            this.txtbIdNumber.PasswordChar = '*';
            this.txtbIdNumber.Size = new System.Drawing.Size(57, 20);
            this.txtbIdNumber.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Forename:";
            // 
            // txtbFirstName
            // 
            this.txtbFirstName.Location = new System.Drawing.Point(74, 6);
            this.txtbFirstName.Name = "txtbFirstName";
            this.txtbFirstName.Size = new System.Drawing.Size(103, 20);
            this.txtbFirstName.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(180, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "MI:";
            // 
            // txtbMiddle
            // 
            this.txtbMiddle.Location = new System.Drawing.Point(206, 6);
            this.txtbMiddle.Name = "txtbMiddle";
            this.txtbMiddle.Size = new System.Drawing.Size(22, 20);
            this.txtbMiddle.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(234, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Surname:";
            // 
            // txtbLastName
            // 
            this.txtbLastName.Location = new System.Drawing.Point(289, 6);
            this.txtbLastName.Name = "txtbLastName";
            this.txtbLastName.Size = new System.Drawing.Size(122, 20);
            this.txtbLastName.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(184, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Primary Phone Number:";
            // 
            // txtbPhoneNo
            // 
            this.txtbPhoneNo.Location = new System.Drawing.Point(308, 39);
            this.txtbPhoneNo.Name = "txtbPhoneNo";
            this.txtbPhoneNo.Size = new System.Drawing.Size(103, 20);
            this.txtbPhoneNo.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Job Description:";
            // 
            // txtbDescription
            // 
            this.txtbDescription.Location = new System.Drawing.Point(98, 27);
            this.txtbDescription.Name = "txtbDescription";
            this.txtbDescription.Size = new System.Drawing.Size(311, 20);
            this.txtbDescription.TabIndex = 7;
            // 
            // rbWorkStudy
            // 
            this.rbWorkStudy.AutoSize = true;
            this.rbWorkStudy.Checked = true;
            this.rbWorkStudy.Location = new System.Drawing.Point(119, 2);
            this.rbWorkStudy.Name = "rbWorkStudy";
            this.rbWorkStudy.Size = new System.Drawing.Size(81, 17);
            this.rbWorkStudy.TabIndex = 12;
            this.rbWorkStudy.TabStop = true;
            this.rbWorkStudy.Text = "&Work Study";
            this.rbWorkStudy.UseVisualStyleBackColor = true;
            // 
            // rbHourly
            // 
            this.rbHourly.AutoSize = true;
            this.rbHourly.Location = new System.Drawing.Point(245, 2);
            this.rbHourly.Name = "rbHourly";
            this.rbHourly.Size = new System.Drawing.Size(55, 17);
            this.rbHourly.TabIndex = 13;
            this.rbHourly.Text = "&Hourly";
            this.rbHourly.UseVisualStyleBackColor = true;
            // 
            // rbContract
            // 
            this.rbContract.AutoSize = true;
            this.rbContract.Location = new System.Drawing.Point(344, 2);
            this.rbContract.Name = "rbContract";
            this.rbContract.Size = new System.Drawing.Size(65, 17);
            this.rbContract.TabIndex = 14;
            this.rbContract.Text = "&Contract";
            this.rbContract.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Classification:";
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(187, 165);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 8;
            this.btnCreate.Text = "C&reate";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(268, 165);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 9;
            this.btnClear.Text = "Clear &All";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(349, 165);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "C&ancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 66);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "E-Mail Address:";
            // 
            // txtbEmail
            // 
            this.txtbEmail.Location = new System.Drawing.Point(99, 66);
            this.txtbEmail.Name = "txtbEmail";
            this.txtbEmail.Size = new System.Drawing.Size(311, 20);
            this.txtbEmail.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtbEmail);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtbPhoneNo);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtbLastName);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtbMiddle);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtbFirstName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtbIdNumber);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(14, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(421, 94);
            this.panel1.TabIndex = 17;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.rbContract);
            this.panel2.Controls.Add(this.rbHourly);
            this.panel2.Controls.Add(this.rbWorkStudy);
            this.panel2.Controls.Add(this.txtbDescription);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(14, 104);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(421, 55);
            this.panel2.TabIndex = 18;
            // 
            // FrmSetup
            // 
            this.AcceptButton = this.btnCreate;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(454, 203);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCreate);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSetup";
            this.Text = "Create an Account...";
            this.Load += new System.EventHandler(this.FrmSetup_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtbIdNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbFirstName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtbMiddle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtbLastName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtbPhoneNo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtbDescription;
        private System.Windows.Forms.RadioButton rbWorkStudy;
        private System.Windows.Forms.RadioButton rbHourly;
        private System.Windows.Forms.RadioButton rbContract;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtbEmail;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}