﻿namespace TimeCard2012 {
    partial class FrmOverClocked {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbTime = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnStaff = new System.Windows.Forms.Button();
            this.rbtnAM = new System.Windows.Forms.RadioButton();
            this.rbtnPM = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(393, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "If you have forgotten to log out, please enter the time you were expected to logo" +
    "ut";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(73, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Format: HH:mm";
            // 
            // txtbTime
            // 
            this.txtbTime.Location = new System.Drawing.Point(159, 36);
            this.txtbTime.Name = "txtbTime";
            this.txtbTime.Size = new System.Drawing.Size(100, 20);
            this.txtbTime.TabIndex = 2;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(331, 62);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnStaff
            // 
            this.btnStaff.Location = new System.Drawing.Point(250, 62);
            this.btnStaff.Name = "btnStaff";
            this.btnStaff.Size = new System.Drawing.Size(75, 23);
            this.btnStaff.TabIndex = 5;
            this.btnStaff.Text = "&Staff";
            this.btnStaff.UseVisualStyleBackColor = true;
            this.btnStaff.Click += new System.EventHandler(this.btnStaff_Click);
            // 
            // rbtnAM
            // 
            this.rbtnAM.AutoSize = true;
            this.rbtnAM.Checked = true;
            this.rbtnAM.Location = new System.Drawing.Point(263, 39);
            this.rbtnAM.Name = "rbtnAM";
            this.rbtnAM.Size = new System.Drawing.Size(41, 17);
            this.rbtnAM.TabIndex = 6;
            this.rbtnAM.TabStop = true;
            this.rbtnAM.Text = "AM";
            this.rbtnAM.UseVisualStyleBackColor = true;
            // 
            // rbtnPM
            // 
            this.rbtnPM.AutoSize = true;
            this.rbtnPM.Location = new System.Drawing.Point(310, 39);
            this.rbtnPM.Name = "rbtnPM";
            this.rbtnPM.Size = new System.Drawing.Size(41, 17);
            this.rbtnPM.TabIndex = 7;
            this.rbtnPM.TabStop = true;
            this.rbtnPM.Text = "PM";
            this.rbtnPM.UseVisualStyleBackColor = true;
            // 
            // FrmOverClocked
            // 
            this.AcceptButton = this.btnStaff;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(422, 96);
            this.Controls.Add(this.rbtnPM);
            this.Controls.Add(this.rbtnAM);
            this.Controls.Add(this.btnStaff);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtbTime);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmOverClocked";
            this.Text = "You May Have Forgotten to Logout";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbTime;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnStaff;
        private System.Windows.Forms.RadioButton rbtnAM;
        private System.Windows.Forms.RadioButton rbtnPM;
    }
}