﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TimeCard2012 {
    public partial class FrmLogin : Form {
        public FrmLogin(string title) {
            this.Text = title;
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnOK_Click(object sender, EventArgs e) {
            if (txtbIDNumber.Text != "") {
                Tag = txtbIDNumber.Text;
                this.DialogResult = DialogResult.OK;
            }
        }

    }// end class FrmLogin
}
